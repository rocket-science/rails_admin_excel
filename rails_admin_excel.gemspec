
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "rails_admin_excel/version"

Gem::Specification.new do |spec|
  spec.name          = "rails_admin_excel"
  spec.version       = RailsAdminExcel::VERSION
  spec.authors       = ["alieksandr-vikhor"]
  spec.email         = ["sasha85ua@gmail.com"]

  spec.summary       = %q{Interface for editing a import excel for rails_admin}
  spec.description   = %q{Rails admin excel}
  spec.homepage      = "https://gitlab.com/rocket-science/rails_admin_excel"
  spec.license       = "MIT"

  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
end
